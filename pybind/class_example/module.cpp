#include <pybind11/operators.h>
#include <pybind11/pybind11.h>
#include <sstream>

namespace py = pybind11;

class point {
public:
  int x = 0, y = 0, z = 0;
  bool operator==(const point &left) const {
    return x == left.x && y == left.y && z == left.z;
  }
};

PYBIND11_MODULE(Cpp, m) {
  py::class_<point>(m, "Point")
      .def(py::init<>())
      .def(py::self == py::self, "test")
      .def_readwrite("x", &point::x)
      .def_readwrite("y", &point::y)
      .def_readwrite("z", &point::z)
      .def("__repr__", [](const point &self) {
        std::stringstream ss;
        ss << "x: " << self.x << ", y: " << self.y << ", z: " << self.z;
        return ss.str();
      })

      ;
}
