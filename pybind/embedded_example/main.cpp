#include <iostream>
#include <pybind11/embed.h>
#include <pybind11/pybind11.h>
#include <thread>

namespace py = pybind11;

class controller {
private:
  controller() {}

public:
  static controller &get() {
    static controller c;
    return c;
  }

  bool stop = false;

  static void init_module(py::module &api) {
    py::class_<controller, std::unique_ptr<controller, py::nodelete>>(api, "Controller")
        .def(py::init([]() { return &(get()); }))
        .def_readwrite("stop", &controller::stop)

        ;
  }
};

PYBIND11_EMBEDDED_MODULE(Api, api) {
  controller::init_module(api);
}

int main() {
  py::scoped_interpreter guard{};

  auto sys = py::module::import("sys");
  sys.attr("path").cast<py::list>().append(PYTHON_PATH);

  auto &controller = controller::get();

  std::thread t([]() {
    py::module::import("stopscript");
  });

  t.detach();

  std::cout << "starting process" << std::endl;
  std::cout << "waiting for cancel signal.";

  while(!controller.stop) {
    std::cout << "." << std::flush;
    sleep(1);
  }

  if(t.joinable()) {
    t.join();
  }
}
